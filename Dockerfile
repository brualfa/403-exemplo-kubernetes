FROM maven:3.6.1-jdk-8-slim
WORKDIR /usr/src/app
RUN apt update && apt install git -y
RUN git clone https://gitlab.com/itau-onda-4/modulo-3/301-investimentos-microservices produto
RUN mvn -f /usr/src/app/produto/produto/pom.xml clean package
ENTRYPOINT ["java", "-jar", "produto/produto/target/produto-0.0.1-SNAPSHOT.jar"]
